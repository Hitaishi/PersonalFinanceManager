﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PFMHT;

namespace PFMHT.Controllers
{
    public class ChecksController : ApiController
    {
        private FinanceManagerEntities2 db = new FinanceManagerEntities2();

        // GET: api/Checks
        public IQueryable<Check> GetChecks()
        {
            return db.Checks;
        }

        // GET: api/Checks/5
        [ResponseType(typeof(Check))]
        public IHttpActionResult GetCheck(int id)
        {
            Check check = db.Checks.Find(id);
            if (check == null)
            {
                return NotFound();
            }

            return Ok(check);
        }

        // PUT: api/Checks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCheck(int id, Check check)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != check.CheckNum)
            {
                return BadRequest();
            }

            db.Entry(check).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CheckExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Checks
        [ResponseType(typeof(Check))]
        public IHttpActionResult PostCheck(Check check)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Checks.Add(check);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CheckExists(check.CheckNum))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = check.CheckNum }, check);
        }

        // DELETE: api/Checks/5
        [ResponseType(typeof(Check))]
        public IHttpActionResult DeleteCheck(int id)
        {
            Check check = db.Checks.Find(id);
            if (check == null)
            {
                return NotFound();
            }

            db.Checks.Remove(check);
            db.SaveChanges();

            return Ok(check);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CheckExists(int id)
        {
            return db.Checks.Count(e => e.CheckNum == id) > 0;
        }
    }
}