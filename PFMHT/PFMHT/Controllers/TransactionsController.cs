﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PFMHT;

namespace PFMHT.Controllers
{
    public class TransactionsController : ApiController
    {
        private FinanceManagerEntities2 db = new FinanceManagerEntities2();

        // GET: api/Transactions
        public IQueryable<Transaction> GetTransactions()
        {
            return db.Transactions;
        }

        [ResponseType(typeof(Transaction))]
        public IQueryable<Transaction> GetTransaction(int id)
        {
            return db.Transactions.Where(x => x.TransactionType == "Credit").Select(x => x);
        }

        [Route("api/Transactions/{str}")]
        public List<Transaction> GetTransactions(string str)
        {
            DateTime s = DateTime.Now;
            string[] tokens = str.Split(',');
            string str1 = tokens[0].Replace("-","/");
            string str2 = tokens[1].Replace("-", "/");
            DateTime frdt = Convert.ToDateTime(str1);
            DateTime todt = Convert.ToDateTime(str2);
            string type = tokens[2];
            List<Transaction> trlist = new List<Transaction>();
            var dsd = (from t in db.Transactions
                       where t.TransferDate >= frdt
                       where t.TransferDate <= todt
                       where t.TransactionType == type
                       select t).ToList<Transaction>();
            //decimal? dr = 0;
            foreach(var a in dsd)
            {
                //dr = dr + a.Amount;
                Transaction tr = new Transaction();
                tr.AccountNum = a.AccountNum;
                tr.Amount = a.Amount;
                tr.Description = a.Description;
                tr.TransactionId = a.TransactionId;
                tr.TransferDate = a.TransferDate;
                tr.TransactionType = a.TransactionType;
                tr.Comments = a.Comments;
                tr.Status = a.Status;
                trlist.Add(tr);
            }
            
            return trlist;
        }
        [Route("api/Transactions/{str}/{type}")]
        public decimal? GetTransactions(string str, string type)
        {
            DateTime s = DateTime.Now;
            string[] tokens = str.Split(',');
            string str1 = tokens[0].Replace("-", "/");
            string str2 = tokens[1].Replace("-", "/");
            DateTime frdt = Convert.ToDateTime(str1);
            DateTime todt = Convert.ToDateTime(str2);
            decimal? dr; 
               var dsd = (from t in db.Transactions where t.TransferDate >= frdt where t.TransferDate <= todt
                       where t.TransactionType == type select t.Amount).ToArray();
            if (dsd.Length!=0)
            {
                dr = dsd.Sum();
            }
            else
                dr = 0;
            return dr;
        }
        // GET: api/Transactions/5
        //[ResponseType(typeof(Transaction))]
        //public IHttpActionResult GetTransaction(int id)
        //{
        //    Transaction transaction = db.Transactions.Find(id);
        //    if (transaction == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(transaction);
        //}

        // PUT: api/Transactions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTransaction(int id, Transaction transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != transaction.TransactionId)
            {
                return BadRequest();
            }

            db.Entry(transaction).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransactionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Transactions
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult PostTransaction(Transaction transaction)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Transactions.Add(transaction);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TransactionExists(transaction.TransactionId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = transaction.TransactionId }, transaction);
        }

        // DELETE: api/Transactions/5
        [ResponseType(typeof(Transaction))]
        public IHttpActionResult DeleteTransaction(int id)
        {
            Transaction transaction = db.Transactions.Find(id);
            if (transaction == null)
            {
                return NotFound();
            }

            db.Transactions.Remove(transaction);
            db.SaveChanges();

            return Ok(transaction);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TransactionExists(int id)
        {
            return db.Transactions.Count(e => e.TransactionId == id) > 0;
        }
    }
}

