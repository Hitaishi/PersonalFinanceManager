﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PFMHT;

namespace PFMHT.Controllers
{
    public class StockHistoriesController : ApiController
    {
        private FinanceManagerEntities2 db = new FinanceManagerEntities2();

        // GET: api/StockHistories
        public IQueryable<StockHistory> GetStockHistories()
        {
            return db.StockHistories;
        }

        // GET: api/StockHistories/5
        [ResponseType(typeof(StockHistory))]
        public IHttpActionResult GetStockHistory(int id)
        {
            StockHistory stockHistory = db.StockHistories.Find(id);
            if (stockHistory == null)
            {
                return NotFound();
            }

            return Ok(stockHistory);
        }

        // PUT: api/StockHistories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStockHistory(int id, StockHistory stockHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != stockHistory.ID)
            {
                return BadRequest();
            }

            db.Entry(stockHistory).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StockHistoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/StockHistories
        [ResponseType(typeof(StockHistory))]
        public IHttpActionResult PostStockHistory(StockHistory stockHistory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.StockHistories.Add(stockHistory);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = stockHistory.ID }, stockHistory);
        }

        // DELETE: api/StockHistories/5
        [ResponseType(typeof(StockHistory))]
        public IHttpActionResult DeleteStockHistory(int id)
        {
            StockHistory stockHistory = db.StockHistories.Find(id);
            if (stockHistory == null)
            {
                return NotFound();
            }

            db.StockHistories.Remove(stockHistory);
            db.SaveChanges();

            return Ok(stockHistory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StockHistoryExists(int id)
        {
            return db.StockHistories.Count(e => e.ID == id) > 0;
        }
    }
}