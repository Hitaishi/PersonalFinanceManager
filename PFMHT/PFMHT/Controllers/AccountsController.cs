﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PFMHT.Models;

namespace PFMHT.Controllers
{
    public class AccountsController : ApiController
    {
        private FinanceManagerEntities2 db = new FinanceManagerEntities2();

        // GET: api/Accounts
        //public IQueryable<Account> GetAccounts()
        //{
        //    return db.Accounts;
        //}
        public List<Account> GetAccounts()
        {
            List<Account> ACT = new List<Account>();
            var dss = (from a in db.Accounts select a).ToList<Account>();
            foreach(var m in dss)
            {
                Account ac = new Account();
                ac.AccountNumber = m.AccountNumber;
                ac.AccountBalance = m.AccountBalance;
                ac.AccountType = m.AccountType;
                ac.CustID = m.CustID;
                ac.Active = m.Active;
                ACT.Add(ac);
            }
            return ACT;
        }
        public List<Account> GetAccounts(int id)
        {
            List<Account> ACT = new List<Account>();
            var dss = (from a in db.Accounts where a.CustID.Equals(id) select a).ToList<Account>();
            foreach (var m in dss)
            {
                Account ac = new Account();
                ac.AccountNumber = m.AccountNumber;
                ac.AccountBalance = m.AccountBalance;
                ac.AccountType = m.AccountType;
                ac.CustID = m.CustID;
                ac.Active = m.Active;
                ACT.Add(ac);
            }
            return ACT;
        }
        [Route("api/Accounts/{id}/{str}")]
        public List<Account> GetAccounts(int id,string str)
        {
            
            List<Account> ACT = new List<Account>();
            var dss = (from a in db.Accounts where a.CustID.Equals(id) where a.Active==str select a).ToList<Account>();
            foreach (var m in dss)
            {
                Account ac = new Account();
                ac.AccountNumber = m.AccountNumber;
                ac.AccountBalance = m.AccountBalance;
                ac.AccountType = m.AccountType;
                ac.CustID = m.CustID;
                ac.Active = m.Active;
                ACT.Add(ac);
            }
            return ACT;
        }
        [Route("api/Accounts/{sm}")]
        public List<Account> GetAccounts(string sm)
        {
            string[] str = sm.Split(',');
            List<Account> ACT = new List<Account>();
            for (int i = 1; i < str.Length; i++)
            {
                var tmp = str[i];
                int aa = int.Parse(str[0]);
                var dss = (from a in db.Accounts where a.CustID.Equals(aa) where a.AccountType==tmp select a).ToList<Account>();
                foreach (var m in dss)
                {
                    Account ac = new Account();
                    ac.AccountNumber = m.AccountNumber;
                    ac.AccountBalance = m.AccountBalance;
                    ac.AccountType = m.AccountType;
                    ac.CustID = m.CustID;
                    ac.Active = m.Active;
                    ACT.Add(ac);
                }
            }
            return ACT;
        }
        [Route("api/Accounts/{sm}/{id}/{st}")]
        public decimal GetSumbal( string sm,int id,string st)
        {
            string[] str = sm.Split(',');
            decimal sumbal = 0;
            for (int i = 1; i < str.Length; i++)
            {
                var tmp = str[i];
                int aa = int.Parse(str[0]);
                var dsd = (from a in db.Accounts where a.CustID.Equals(aa) where a.AccountType == tmp select a.AccountBalance).ToArray();
                if (dsd.Length!=0)
                {
                    sumbal = sumbal + dsd.Sum();
                }
                else
                {
                    sumbal = sumbal+0;
                }
            }
            return sumbal;
        }

        // GET: api/Accounts/5
        //[ResponseType(typeof(Account))]
        //public IHttpActionResult GetAccount(int id)
        //{
        //    Account account = db.Accounts.Find(id);
        //    if (account == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(account);
        //}

        //PUT: api/Accounts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAccount(int id, Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != account.AccountNumber)
            {
                return BadRequest();
            }

            db.Entry(account).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Accounts
        //public void PostAccount(Account account)
        //{
        //    var list = (from a in db.Accounts select a).ToList<Account>();
        //    foreach(var m in list)
        //    {
        //        if (account.AccountNumber == m.AccountNumber)
        //        {
        //            if (account.AccountType == m.AccountType)
        //            {
        //                var ls = from a in db.Accounts where a.AccountNumber.Equals(account.AccountNumber) select a;
        //                foreach(Account ac in ls)
        //                {
        //                    ac.AccountBalance = account.AccountBalance;
        //                    ac.Active = account.Active;
        //                    ac.CustID = account.CustID;
        //                }
        //                try
        //                {
        //                    db.SaveChanges();
        //                }
        //                catch (DbUpdateConcurrencyException)
        //                {
        //                    if (!AccountExists(m.AccountNumber))
        //                    {
        //                         NotFound();
        //                    }
        //                    else
        //                    {
        //                        throw;
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            db.Accounts.Add(account);
        //            try
        //            {
        //                db.SaveChanges();
        //            }
        //            catch (DbUpdateException)
        //            {
        //                if (AccountExists(account.AccountNumber))
        //                {
        //                     Conflict();
        //                }
        //                else
        //                {
        //                    throw;
        //                }
        //            }
        //        }
        //    }
        //}

        //[ResponseType(typeof(Account))]
        public IHttpActionResult PostAccount(Account account)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Accounts.Add(account);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AccountExists(account.AccountNumber))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = account.AccountNumber }, account);
        }

        // DELETE: api/Accounts/5
        [ResponseType(typeof(Account))]
        public IHttpActionResult DeleteAccount(int id)
        {
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return NotFound();
            }

            db.Accounts.Remove(account);
            db.SaveChanges();

            return Ok(account);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountExists(int id)
        {
            return db.Accounts.Count(e => e.AccountNumber == id) > 0;
        }
    }
}