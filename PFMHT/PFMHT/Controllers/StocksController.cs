﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PFMHT;

namespace PFMHT.Controllers
{
    public class StocksController : ApiController
    {
        private FinanceManagerEntities2 db = new FinanceManagerEntities2();

        // GET: api/Stocks
        public IQueryable<Stock> GetStocks()
        {
            return db.Stocks;
        }
        public List<Stock> GetStocks(int id)
        {
            var ds= db.Proc_Stockgrid(id);
            List<Stock> stlist = new List<Stock>();
            foreach(var a in ds)
            {
                Stock s = new Stock();
                s.Amount = (decimal)a.Amount;
                s.Date = a.Date;
                s.Price = a.Price;
                s.StockName = a.StockName;
                s.Quantity = a.Quantity;
                s.Status = a.Status;
                stlist.Add(s);
            }
            return stlist;
        }
        [Route("api/Stocks/{id}/{str}/{st}")]
        public List<Stock> GetStocks(int id,string str,string st)
        {
            List<Stock> stlist = new List<Stock>();
            var ds = (from s in db.Stocks where s.CustID.Equals(id) where s.Status == str select s).ToList();
            foreach (var a in ds)
            {
                Stock s = new Stock();
                s.Amount = (decimal)a.Amount;
                s.Date = a.Date;
                s.Price = a.Price;
                s.StockName = a.StockName;
                s.Quantity = a.Quantity;
                s.Status = a.Status;
                stlist.Add(s);
            }
            return stlist;
        }
        [Route("api/Stocks/{str}/{id}")]
        public decimal? Getstockbal(string str,int id)
        {
            
            var dss = (from a in db.Stocks where a.CustID.Equals(id) select a.Amount).ToArray();
            if (dss.Length!=0)
            {
                return dss.Sum();
            }
            else
            {
                return 0;
            }
        }
        // GET: api/Stocks/5
        //[ResponseType(typeof(Stock))]
        //public IHttpActionResult GetStock(int id)
        //{
        //    Stock stock = db.Stocks.Find(id);
        //    if (stock == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(stock);
        //}

        // PUT: api/Stocks/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutStock(int id, Stock stock)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != stock.StockID)
            {
                return BadRequest();
            }

            db.Entry(stock).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StockExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Stocks
        [ResponseType(typeof(Stock))]
        public IHttpActionResult PostStock(Stock stock)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Stocks.Add(stock);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (StockExists(stock.StockID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = stock.StockID }, stock);
        }

        // DELETE: api/Stocks/5
        [ResponseType(typeof(Stock))]
        public IHttpActionResult DeleteStock(int id)
        {
            Stock stock = db.Stocks.Find(id);
            if (stock == null)
            {
                return NotFound();
            }

            db.Stocks.Remove(stock);
            db.SaveChanges();

            return Ok(stock);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool StockExists(int id)
        {
            return db.Stocks.Count(e => e.StockID == id) > 0;
        }
    }
}