﻿//var currencyModule = angular.module('currencyApp', []);
myApp.controller('currencyController', function ($scope, currencyService) {
    
    $scope.fromcurrval = 2;
    $scope.fromcurrency = "USD";
    $scope.tocurrency = "INR";
    $scope.loading = true;
    $scope.update = function () {
        if ($scope.fromcurrency == $scope.tocurrency) {
            $scope.ourcurrvalue = $scope.fromcurrval;
        }
        else if ($scope.fromcurrval == "") {
            $scope.ourcurrvalue = "";
        }
        else {
            $scope.loading = true;
            
            currencyService.calculate($scope.fromcurrency, $scope.fromcurrval, $scope.tocurrency).then(function (response) {
                $scope.outcurrvalue = response.data;
                $scope.loading = false;
            });
        }
    }
});
//factory

myApp.factory('currencyService', function ($http) {
    return {
        calculate: function (fromCurrCode, fromCurrVal, toCurrCode) {
            return $http.get('http://localhost:53816/api/Currency/convertcurrency/' + fromCurrCode + '/' + fromCurrVal + '/' + toCurrCode);
            
        }
    }
});
//Directive for the input to be numeric
myApp.directive('isNumber', function (currencyService) {
    return {
        require: 'ngModel',
        link: function (scope) {
            scope.$watch('fromcurrval', function (newValue, oldValue) {
                var arr = String(newValue).split("");
                if (arr.length == 0) {
                    scope.outcurrvalue = "";
                }
                else if (arr.length == 1 && (arr[0] == '.')) return;
                else if (isNaN(newValue)) {
                    scope.fromcurrval = oldValue;
                }
                else {
                    if (scope.fromcurrency == scope.tocurrency) {
                        scope.outcurrvalue = scope.fromcurrval;
                    }
                    else {
                        scope.loading = true;
                        currencyService.calculate(scope.fromcurrency, scope.fromcurrval, scope.tocurrency).then(function (outcurrvalue) {
                            scope.outcurrvalue = outcurrvalue;
                            console.log('hi');
                            scope.loading = false;
                        });
                    }
                }
            });
        }

    };
});
