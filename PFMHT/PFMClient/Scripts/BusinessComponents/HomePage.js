﻿var myApp = angular.module("myApp", ["ui.router", "chart.js", "ui.grid", "ngTouch", "ngAnimate", "ui.bootstrap","kendo.directives"]);
myApp.config(function ($stateProvider, $urlRouterProvider) {
    
    $stateProvider
        .state("Home", {//added by LNS for the home page
            url: "/home",
            templateUrl: 'CarouselHome.html',
            controller:'HomeController'
        })
.state('CurrencyConverter', {
    url: '/CurrencyConverter',
    templateUrl: 'CurrencyConverter.html',
    //templateUrl: 'E:\Summit\HiteshiProject\PFMHT\PFMClient\Modified Html\CurrencyConverterMod.html',
    controller: "currencyController"
})
.state('ExpensesAccount', {
    url: '/ExpensesAccount',     
    templateUrl: 'ExpensesAccount.html',
    controller: "expensesController"
})
    .state('PrintChecks', {
        url: '/PrintChecks',
        templateUrl: 'PrintChecks.html',
        controller: "Binder"
    })
    .state('AccountSettings', {
        
        url: '/AccountSettings',
        templateUrl: 'Vamsi/AccountSettings.html'
    })
    .state('UserProfile', {
        url: '/AccountSettings/UserProfile',
        templateUrl: 'Vamsi/userprof.html',
        controller: "UPController"
    })
    .state('AccountManager', {
        url: '/AccountSettings/AccountManager',
        templateUrl: 'Vamsi/accmang.html',
        controller: 'AMController'
    })
    .state('Reminders', {
        url: '/Reminders',
        templateUrl: 'Reminders.html',
        controller: "MyCtrl"
    })
    .state('StockManager', {
        url: '/StockManager',
        templateUrl: 'Vamsi/Stocks.html',
        controller: "STController"
    })
    .state('Reports', {
        url: '/Reports',
        templateUrl: 'Vamsi/Reports.html',
        controller: "RPController"
    })
    .state('Error', {
        url: '/Error',
        templateUrl:'Vamsi/ErrorPage.html'
    })
.state('Reminders', {
    url: '/Reminders',
    templateUrl: 'Reminders.html',
    controller: 'MyCtrl'
     .state('AccountRegister', {
         url: '/AccountRegister',
         templateUrl: 'AccountRegister.html',
         controller: 'TransactionsController'
     })
      .state('AccountGraphingIncome', {
          url: '/AccountGraphingIncome',
          templateUrl: 'AccountGraphingIncome.html',
          controller: 'incomeController'
})

    $urlRouterProvider.otherwise("/home")//added by LNS
});