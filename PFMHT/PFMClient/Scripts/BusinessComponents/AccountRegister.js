﻿myApp.controller("TransactionsController", function ($scope, Transactions, $http, $window) {
    $scope.Transactions = Transactions;

    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    }

    $scope.getTran = function () {
        debugger;
        $http.get("http://localhost:53816/api/Transactions").then(function (response) {
            debugger;
            $scope.Transactions = response.data;
        });
    };

    //$scope.dateRanges = {
    //    all: { from: 0, to: Number.MAX_VALUE },
    //    // defining getCurrent[Week,Month,Year]Range() left open for you,
    //    // https://stackoverflow.com/questions/8381427/ is a good start
    //    week: getCurrentWeekRange(),
    //    month: getCurrentMonthRange(),
    //    year: getCurrentYearRange(),
    //};

    //$scope.currentDateRange = $scope.dateRanges.all; // as initial value
    //$scope.eventDateFilter = function (event) {
    //    debugger;
    //    return $scope.currentDateRange.from <= event.TransferDate
    //        && event.TransferDate <= $scope.currentDateRange.to;
    //};

    $scope.AddNewTran = function () {
        debugger;
        var data = {
            Description: $scope.Transactions.Description,
            TransactionType: $scope.Transactions.TransactionType,
            Amount: $scope.Transactions.Amount,
            TransferDate: $scope.Transactions.TransferDate,
            Status: $scope.Transactions.Status,
            Comments: $scope.Transactions.Comments,
            AccountNum: $scope.Transactions.AccountNum
        }

        $http.post("http://localhost:53816/api/Transactions", data).then(function (result) {
            console.log("inserted Successfully");
        },
            function (err) {
                console.log("Not inserted Successfully");
            });
        $window.location.reload();

    };


    $scope.Update = function (AccountNum, TransactionType, Description, Amount, TransferDate, Comments, Status, TransactionId) {
        debugger;
        var data = {
            TransactionId: TransactionId,
            Status: Status,
            Description: Description,
            TransactionType: TransactionType,
            Amount: Amount,
            TransferDate: TransferDate,
            Comments: Comments,
            AccountNum: AccountNum
        }

        $http.put('http://localhost:53816/api/Transactions/' + data.TransactionId, data).then(function (result) {
            console.log("Successfully");
        }, function (err) {
            console.log("Not Successfully");
        });
    }

});

myApp.service("Transactions", Transactions);

function Transactions() {
    this.TransactionId = "";
    this.Description = "";
    this.TransactionType = "";
    this.Amount = "";
    this.TransferDate = "";
    this.Status = "";
    this.Comments = "";
    this.AccountNum = "";
}

//function getCurrentDayRange() {
//    return {
//        from: moment().startOf('day'),
//        to: moment().endOf('day')
//    };
//}
//function getCurrentWeekRange() {
//    return {
//        from: moment().startOf('week'),
//        to: moment().endOf('week')
//    };
//};
//function getCurrentMonthRange() {
//    return {
//        from: moment().startOf('month'),
//        to: moment().endOf('month')
//    };
//}

//function getCurrentYearRange() {
//    return {
//        from: moment().startOf('year'),
//        to: moment().endOf('year')
//    };
//}    