﻿//var ExpensesModule = angular.module('ExpensesApp', ["chart.js"]);

myApp.controller('expensesController', function ($scope, $http) {
    $scope.requestData = function () {
        debugger;
        var arrData = [];
        // var arrColors = [];
        
        $http.get('http://localhost:53816/api/ExpensesAccount').then(function (response) {

            //arrData.push(response.data.value);
            $.map(response.data, function (item) {
                arrData.push(item.Entertainment);
                arrData.push(item.Organization);
                arrData.push(item.Shopping);
                arrData.push(item.Stocks);
                arrData.push(item.Travel);
            })
            var data1 = {
                labels: ["Entertainment", "Organization", "Shopping", "Stocks", "Travel"],
                datasets: [{
                    backgroundColor: [
                      "rgba(77,83,96,0.5)",
                      "rgba(253,180,92,0.5)",
                      "rgba(255, 0, 0, 0.8)",
                      "rgba(255, 40, 44, 0.5)",
                      "rgba(96, 255, 173, 0.5)"
                    ],
                    data: arrData
                }]
            }

            var myCanvas = new Chart('myCanvas', {
                type: 'pie',
                data: data1
            });

            $scope.toggle = function () {
                
             var myCanvas = new Chart('myCanvas', {
                 type: 'bar',
                 data: data1
             });
            }
        });
        
        console.log(arrData);
        // $scope.data = [];
        // $scope.labels = ["Travel", "Organization", "Shopping", "Entertainment", "Stocks"];
        // $scope.data.push(arrData);
        // $scope.data.push(labels);

    }
    
    //$scope.arrayColors2 = [
    //{

    //    backgroundColor: [
    //      "rgba(77,83,96,0.5)",
    //      "rgba(253,180,92,0.5)",
    //      "rgba(255, 0, 0, 0.8)",
    //      "rgba(255, 40, 44, 0.5)",
    //      "rgba(96, 255, 173, 0.5)"

    //    ],
    //    hoverBackgroundColor: [
    //      "rgba(0, 0, 0, 0.5)",
    //      "rgba(253,180,92,0.5)",
    //      "rgba(255, 0, 0, 0.8)",
    //      "rgba(255, 40, 44, 0.5)",
    //      "rgba(96, 255, 173, 0.5)"

    //    ]
    //}
    //]
    $scope.printToCart = function () {
        const canvas = document.getElementById('myCanvas');
        const imgWrap = document.getElementById('imgWrap');
        var img = new Image();
        img.src = canvas.toDataURL();
        imgWrap.appendChild(img);
        canvas.style.display = 'none';
        //var innerContents = canvas.innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()"><img style="width:300px;height:300px" src="' + canvas.toDataURL() + '"></img></body></html>');
        popupWinindow.document.close();
    }
   
});

