﻿myApp.controller("incomeController", function ($scope, IncomeTran, $http) {

    $scope.IncomeTran = IncomeTran;

    $scope.printToCart = function (printSectionId) {
        var innerContents = document.getElementById(printSectionId).innerHTML;
        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
        popupWinindow.document.open();
        popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
        popupWinindow.document.close();
    }

    $scope.getIncomeTran = function () {

        var arrAmount = [];
        var arrDes = [];
        $http.get("http://localhost:53816/api/Transactions/1").then(function (response) {
            debugger;

            $.map(response.data, function (item) {
                arrAmount.push(item.Amount);
                arrDes.push(item.Description);
            })

            var data1 = {
                labels: arrDes,
                datasets: [{
                    backgroundColor: [
                        "rgba(77,83,96,0.5)",
                        "rgba(253,100,92,0.5)",
                        "rgba(255,0,2,0.5)"
                    ],
                    data: arrAmount
                }]
            }

            var myCanvas = new Chart('line', {
                type: 'line',
                data: data1
            })

            //$scope.onClick = function (points, evt) {
            //    console.log(points, evt);
            //};
        });
    };
    //$scope.labels = [IncomeTran.TransferDate];



    $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
    $scope.options = {
        scales: {
            yAxes: [
                {
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left'
                }
            ]
        }
    };
});

myApp.service("IncomeTran", IncomeTran);

function IncomeTran() {
    this.TransactionId = "";
    this.Description = "";
    this.TransactionType = "";
    this.Amount = "";
    this.TransferDate = "";
    this.Status = "";
    this.AccountNum = "";
}