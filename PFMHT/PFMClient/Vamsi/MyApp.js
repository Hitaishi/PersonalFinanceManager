﻿/// <reference path="C:\Users\hitai\Source\Repos\PersonalFinanceManager\PFMHT\PFMClient\Scripts/BusinessComponents/HomePage.js" />
//var myApp = angular.module('App', ['ngRoute', 'ui.grid', 'ngTouch', 'ngAnimate', 'ui.bootstrap']);
myApp.controller('HomeController', ['$scope', function () { }]);

    myApp.controller("UPController", ['$scope', 'GetCustomer', 'SetCustomer', function ($scope, GetCustomer, SetCustomer) {
        var crud = GetCustomer.getcustomer();
        crud.then(function (result) {
            if (result) {
                $scope.Name = result.CustName;
                $scope.Address = result.Address;
                $scope.Phone = result.Phone;
                $scope.Email = result.Email;
            }
        });
        $scope.Save = function () {
        
            var customer = {
                CustID: 101,
                CustName: $scope.Name,
                Address: $scope.Address,
                Phone: $scope.Phone,
                Email: $scope.Email
            }
            console.log(customer);
            var ccc = SetCustomer.Put(customer);
            ccc.then(function (result) {
                $window.location.reload();
            
            }, function (err) {
                $location.url('/Error');
            });
        }
    }]);
    myApp.controller("AMController", ['$scope', '$http', '$window', '$location', function ($scope, $http, $window, $location) {
        $http.get('http://localhost:53816/api/AccountType1').then(function (result) {
            $scope.accs = result.data;
        }, function (err) {
            $location.url('/Error');
        });
        var aid ={ id:101,str:'Yes'};
        $http.get('http://localhost:53816/api/Accounts/' + aid.id +"/"+aid.str).then(function (res) {
            $scope.myData = res.data;
        }, function (err) {
            $location.url('/Error');
        });
        $http.get('http://localhost:53816/api/Accounts/'+aid.id).then(function (result) {
            $scope.accnums = result.data;
        }, function (err) {
            $location.url('/Error');
        });
        $scope.activaccs = [{
            Active:'Yes'
        }, { Active: 'No' }]
        $scope.onAccnumberchange = function (accountNumber) {
            $scope.ACCTTYPE = $scope.accountNumber.AccountType;
            $scope.ACCTBAl = $scope.accountNumber.AccountBalance;
        }
        $scope.onchangeActive = function () {
            $scope.acdrdo = $scope.active1.Active;
        }
        $scope.onchangeAddactive = function () {
            $scope.version = $scope.active.Active;
        }
        $scope.Update = function () {
            var detail = {
                AccountNumber: $scope.accountNumber.AccountNumber,
                AccountType: $scope.accountNumber.AccountType,
                AccountBalance: $scope.accountNumber.AccountBalance,
                Active: $scope.acdrdo,
                CustID:id
            }
            $http.put('http://localhost:53816/api/Accounts/'+detail.AccountNumber, detail).then(function (result) {
                $window.location.reload();
            }, function (err) {
                $location.url('/Error');
            });
        };
        $scope.Save = function () {
            debugger;
            var detail = {
                AccountNumber:$scope.accno,
                AccountType:$scope.acctype.AccountType,
                AccountBalance: $scope.accbal,
                Active:$scope.version,
                CustID:101
            }
            $http.post('http://localhost:53816/api/Accounts', detail).then(function (result) {
                $window.location.reload();
            }, function (err) {
                $location.url('/Error');
            });
        }
        $scope.gridOptions = {
            enableFilter: true,
            data: 'myData',
            columnDefs: [{ field: 'AccountNumber', displayName: 'Account Number'},
            { field: 'AccountType', displayName: 'Account Type' },
            { field: 'AccountBalance', displayName: 'Account Balance', filter: "currency", cellFilter: 'currency' },
            { field: 'Active', displayName: 'Active' }]
        };
    }]);
    myApp.service("GetCustomer", ['$http', function ($http) {
        var id = 101;
        this.getcustomer = function () {
            return $http({
                method: 'GET',
                url: 'http://localhost:53816/api/Customers/'+id,
                headers: { 'Content-Type': 'application/Json' },
            }).then(function (res) {
                return res.data;
            })
        }
    }]);
    myApp.service("SetCustomer", ['$http', function ($http) {
        this.Put = function (customer) {
            return $http({
                method: 'PUT',
                url: 'http://localhost:53816/api/Customers/' + customer.CustID,
                data: customer,
                headers: { 'Content-Type': 'application/Json' },
            })
        }
    }]);
    myApp.controller("STController", function ($scope, $http, $window,$location) {
        var id = 101;
        //var detail = {
        //    id: 101,
        //    str:typelist
        //}
        var detail = "101,Savings,Checkings";
        $http.get('http://localhost:53816/api/Accounts/'+detail).then(function (result) {
            $scope.accnums = result.data;
        }, function (err) {
            $location.url('/Error');
        });
        $scope.stockdata = '';
        $http.get('Vamsi/Stocks.json').then(function (data) {
            $scope.stockdata = data.data;
        }, function (err) {
            $location.url('/Error');
        });
        $http.get('http://localhost:53816/api/Stocks/'+id).then(function (res) {
            $scope.myData = res.data;
        }, function (err) {
            $location.url('/Error');
        });
        $scope.gridStocks = {
            data: 'myData',
            columnDefs: [{ field: '$id', displayName: 'ID' },
                {field:'StockName', displayName:'Stock Name'},
                { field: 'Price', displayName: 'Price', cellFilter: 'currency' },
                { field: 'Quantity', displayName: 'Quantity' },
                { field: 'Amount', displayName: 'Amount', cellFilter: 'currency' },
                { field: 'Date', displayName: 'Date' },
                {field:'Status',displayName:'Status'}
            ]
        };
        $scope.stknamechange = function (stkname) {
            $scope.stckprice = $scope.stkname.Price;
        };
        $scope.onAccnumberchange = function (accountNumber) {
        
        };
        $scope.Update = function () {
            var bal = $scope.stckprice * $scope.quant;
            if ($scope.accountNumber.AccountBalance < bal) {
                alert("There is not enough balance in this account. please select another account.");
            }
            var d=new Date();
            var detail = {
                Description:"Stocks",
                AccountNum: $scope.accountNumber.AccountNumber,
                TransactionType: "Debit",
                Amount: $scope.stckprice * $scope.quant,
                TransferDate:d.getMonth()+'/'+d.getDate()+'/'+d.getFullYear(),
                CustID: id
            }
        
            var stdetail = {
                StockID: parseInt(Math.random() * 1000),
                StockName: $scope.stkname.name,
                Price: $scope.stkname.Price,
                Quantity: $scope.quant,
                Amount: $scope.stckprice * $scope.quant,
                Date: d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear(),
                CustID: id,
                Status:'Bought'
            }
            $http.post('http://localhost:53816/api/Stocks', stdetail).then(function (result) {
                $http.post('http://localhost:53816/api/Transactions', detail).then(function (result) {
                    $window.location.reload();
                });
            }, function (err) {
                $location.url('/Error');
            });
        }  
    });
    myApp.controller("RPController", function ($scope, $http,$location) {
        var id = 101;
        $scope.Search = function () {
            var date1y = ($scope.fromdate).getFullYear();
            var date1m = (($scope.fromdate).getMonth()) + 1;
            var date1d = ($scope.fromdate).getDate();
            var datefr = date1m + '-' + date1d + '-' + date1y;
            var date2y = ($scope.todate).getFullYear();
            var date2m = (($scope.todate).getMonth()) + 1;
            var date2d = ($scope.todate).getDate();
            var dateto = date2m + '-' + date2d + '-' + date2y;
            var str = datefr + ',' + dateto + ',Debit';
            console.log(str);
            var str1 = datefr + ',' + dateto + ',Credit';
            $scope.DebData = '';
            $http.get('http://localhost:53816/api/Transactions/' + str).then(function (result) {
                $scope.DebData = result.data;
                $http.get('http://localhost:53816/api/Transactions/' + str + '/' + 'Debit').then(function (res) {
                    $scope.SumDebt = res.data;
                });
            }, function (err) {
                $location.url('/Error');
            });
            $http.get('http://localhost:53816/api/Transactions/' + str1).then(function (result) {
                $scope.CreditData = result.data;
                $http.get('http://localhost:53816/api/Transactions/' + str1 + '/' + 'Credit').then(function (res) {
                    $scope.SumCredit = res.data;
                });
            }, function (err) {
                $location.url('/Error');
            });
        }
        $scope.gridDebit = {
            data: 'DebData',
            columnDefs: [{ field: 'Description', displayName: 'Description' },
            { field: 'Amount', displayName: 'Amount', cellFilter :'currency'},
            {field:'TransferDate',displayName:'TransactionDate'}]
        }
        $scope.gridCredit = {
            data: 'CreditData',
            columnDefs: [{ field: 'Description', displayName: 'Description' },
            { field: 'Amount', displayName: 'Amount', cellFilter: 'currency' },
            { field: 'TransferDate', displayName: 'TransactionDate' }]
        }
        var a;
        var b;
        var c;
        var detail = "101,Savings,Checkings";
        $http.get('http://localhost:53816/api/Accounts/' + detail).then(function (result) {
            $scope.accschksav = result.data;
            $http.get('http://localhost:53816/api/Accounts/' + detail + '/' + id + '/' + str).then(function (res) {
                debugger;
                $scope.chksavbal = res.data;
                a = parseFloat(res.data);
            });
        }, function (err) {
            $location.url('/Error');
        });
        var det = "101,Loan,Credit Card";
        $http.get('http://localhost:53816/api/Accounts/' + det).then(function (result) {
            $scope.accsloan = result.data;
            $http.get('http://localhost:53816/api/Accounts/' + det + '/' + id + '/' + str).then(function (res) {
                $scope.loanbal = res.data
                b = parseFloat(res.data);
            });
        }, function (err) {
            $location.url('/Error');
        });
        var str='101';
        $http.get('http://localhost:53816/api/Stocks/' + id+'/'+'Bought'+'/'+str).then(function (res) {
            $scope.myData = res.data;
            $http.get('http://localhost:53816/api/Stocks/' + str+'/'+id).then(function (response) {
                $scope.stockbal = response.data;
                c = parseFloat(response.data);
                $scope.Netbal = a + c - b;
            });
        }, function (err) {
            $location.url('/Error');
        });
        debugger;
         
        
        $scope.gridAccs = {
            data: 'accschksav',
            columnDefs: [{ field: 'AccountNumber', displayName: 'Account Number' },
        { field: 'AccountType', displayName: 'Account Type' },
        { field: 'AccountBalance', displayName: 'Account Balance', filter: "currency", cellFilter: 'currency' }]
        }
        $scope.gridStcks = {
            data: 'myData',
            columnDefs: [{ field: 'StockName', displayName: 'Stock Name' },
            { field: 'Date', displayName: 'Date' },
            { field: 'Amount', displayName: 'Amount', cellFilter: 'currency' } ]
        }
        $scope.gridLoan = {
            data: 'accsloan',
            columnDefs: [{ field: 'AccountNumber', displayName: 'Account Number' },
        { field: 'AccountType', displayName: 'Account Type' },
        { field: 'AccountBalance', displayName: 'Account Balance', filter: "currency", cellFilter: 'currency' }]
        }
    });